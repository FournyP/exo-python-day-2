import itertools

def powerset(A):

    all_combi = []
    for temp in range(len(A) +1):
        combi= itertools.combinations(A, temp)
        combi_list = list(combi)
        all_combi += combi_list

    return all_combi
        


if __name__ == "__main__":
    
    res = powerset([1, 2, 3])
    print(res)