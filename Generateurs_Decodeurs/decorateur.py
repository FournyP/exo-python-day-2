from functools import wraps

def countable(fn):
    @wraps(fn)
    def wrapped(*args, **kwargs):
        return fn(*args) + 1
    return wrapped

@countable
def mafonction(number):
    return number

if __name__ == "__main__":
    counter = 0
    counter = mafonction(counter)
    print(counter)
    counter = mafonction(counter)
    print(counter)