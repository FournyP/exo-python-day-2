def askNumber():
    try:
        return int(input('Saisir un nombre : '))
    except:
        print("La valeur n'est pas un nombre")
        return askNumber()

def generateAlplabet():

    alphabet = []

    for intLetter in range(97, 123):
        alphabet.append(chr(intLetter))

    return alphabet

def generateNAlplabet(nb):

    alphabets = []

    for index in range(0, nb):
        alphabets = alphabets + generateAlplabet()

    return ", ".join("".join(sorted(alphabets)))

if __name__ == "__main__":
    
    number = askNumber()
    alphabet = generateNAlplabet(number)

    print(alphabet)