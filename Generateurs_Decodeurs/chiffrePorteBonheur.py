def askNumber():
    try:
        return int(input('Saisir un nombre : '))
    except:
        print("La valeur n'est pas un nombre")
        return askNumber()


def convertNumberToDigit(number):
    
    if number == 0:
        return [0]

    l = []
    while number > 0:
        l.append(number % 10)
        number = number // 10
    return l

def calculSquareOfEach(nb):
    digits = convertNumberToDigit(nb)
    res = 0
    for digit in digits:
        res = res + digit**2

    return res


def chiffrePorteBonheur(nb):
    
    initalNb = nb

    while(nb > 10):
        nb = calculSquareOfEach(nb)

    if(nb == 1):
        print("Le nombre " + str(initalNb) + " est un chiffre porte bonheur.")
    else:
        print("Le nombre " + str(initalNb) + " n'est pas un chiffre porte bonheur.")

if __name__ == "__main__":
    
    number = askNumber()
    chiffrePorteBonheur(number)