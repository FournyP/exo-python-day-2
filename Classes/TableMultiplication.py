class TableMultiplication:

    def __init__(self, table):
        if(isinstance(table, int) == False):
            raise "Invalid argument"

        self.table = table
        self.currentMultiplier = -1

    def prochain(self):
        self.currentMultiplier = self.currentMultiplier + 1
        return self.table * self.currentMultiplier

    def __str__(self):
        return "Table : " + str(self.table)

    def __repr__(self):
        return self.__str__()
