class Domino:

    def __init__(self, firstSide, secondSide):

        if(isinstance(firstSide, int) == False or isinstance(secondSide, int) == False):
            raise "Invalid arguments"

        self.firstSide = firstSide
        self.secondSide = secondSide

    def __str__(self):
        return "Face A: " + str(self.firstSide) + ", Face B: " + str(self.secondSide)

    def affiche_point(self):
        print(self.__str__())

    def totale(self):
        return self.firstSide + self.secondSide

    def __repr__(self):
        return self.__str__()