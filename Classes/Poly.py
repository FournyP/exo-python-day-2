import itertools 

class Poly:

    def __init__(self, *args):
        if(isinstance(args, tuple) == False):
            raise "Invalid arguments"        

        self.coeff = []
        for value in args:
            self.coeff.append(value)

    def evalue(self, x):
        res = 0
        
        for counter, value in enumerate(self.coeff):
            res = res + (value * x**counter)

        return res        

    def __add__(self, other):
        coeff = []

        for combinedValue in itertools.zip_longest(self.coeff, other.coeff, fillvalue='_'):
            res = 0
            for value in combinedValue:
                if value != '_':
                    res = res + value

            coeff.append(res)
    
        p = Poly(*coeff)
        return p