class Fraction:

    def __init__(self, numerator, denominator):

        if(isinstance(numerator, int) == False or isinstance(denominator, int) == False):
            raise "Invalid arguments"

        self.numerator = numerator
        self.denominator = denominator
        
    def affiche(self):
        print(self.__str__())

    def pgcd(self, a, b):
        if b==0:
            return a
        else:
            r=a%b
            return self.pgcd(b,r)

    def minimise(self):
        pgcd = self.pgcd(self.numerator, self.denominator)
        self.numerator = int(self.numerator / pgcd)
        self.denominator = int(self.denominator / pgcd)

    def __add__(self, other):
        denominator = (self.denominator * other.denominator)
        numerator = ((self.numerator * other.denominator) + (other.numerator * self.denominator))

        fraction = Fraction(numerator, denominator)
        fraction.minimise()

        return fraction 

    def __sub__(self, other):
        denominator = (self.denominator * other.denominator)
        numerator = ((self.numerator * other.denominator) - (other.numerator * self.denominator))

        fraction = Fraction(numerator, denominator)
        fraction.minimise()

        return fraction 

    def __mul__(self, other):
        denominator = (self.denominator * other.denominator)
        numerator = (self.numerator * other.numerator)

        fraction = Fraction(numerator, denominator)
        fraction.minimise()
        
        return fraction

    def __div__(self, other):
        denominator = (self.denominator / other.denominator)
        numerator = (self.numerator / other.numerator)

        fraction = Fraction(numerator, denominator)
        fraction.minimise()
        
        return fraction

    def __str__(self):
        return str(self.numerator) + "/" + str(self.denominator)

    def __repr__(self):
        return self.__str__()